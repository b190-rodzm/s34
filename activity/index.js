// npm init
/*	- using this command will make a "package.json" in our repo
	  - package.json tracks the version of our application, depending on the settings we have set. also, we can see the dependencies that we have installed inside of this file
*/
/*
	npm install express
		-after triggering this command, express will now be listed as a "dependency". this can be seen inside the "package.json" file under the "dependencies" property
		- will create  "node_modules" folder and package-lock.json
			"node_modules" directory should be left on the local repository because some of the hosting sites will fail to load the repository once it found the "node_modules" inside the repo. another reason where node_modules is left on the local repository is it takes too much time to commit
			"node_modules" is also where the dependencies needed files are stored.
		".gitignore" files, as the name suggests will tell the git what files are to be spared/"ignored" in terms of commiting and pushing.

		"npm install" - this command is used when there are available dependencies inside our "package.json" but are not yet installed inside the repo/project - this is useful whentrying to clone a repo from a git repository to our local repository
*/



// we need now the express module since in this dependency, it has already built in codes that will allow the dev to create a server in a breeze/easier
const express = require("express"); // allows us to create our server using express
const app = express(); // he app variable is our server
const port = 3000; // setting up port variable
// methods used from express middlewares (software that provide common services and capabilities for the application):
app.use(express.json()); // app.use lets the server to handle json data types from requests
app.use(express.urlencoded({extended: true})); //allows the server to read data from forms


// SECTION-ROUTES
// GET method
app.get("/", (req, res) => {
	res.send("Hello World");
});

app.get("/hello", (req, res) => { 	//app.get(["/hello","/test"], (req, res) => {} - if same content but using different URIs
	res.send("Hello from /hello endpoint");
});

// POST method
app.post("/hello", (req, res) =>{
	res.send(`Hello there ${req.body.firstName} ${req.body.lastName}!`); 	// req.body contains the contents/data of the request body
});

let users = [];

app.post("/signup", (req, res) => {
	console.log(req.body);
	/*
		create an if-else statement stating that:
			- if the firstName and lastName are not null, 
				the object will be pushed into the users array;
				the server will send a message confirming that the user has signed up successfully;

			- else, send a message stating that firstName and lastName should be filled with information
	*/
	if (req.body.firstName !== "" && req.body.lastName !== "") {
		users.push(req.body);
		res.send( `User ${req.body.firstName} ${req.body.lastName} has signed up successfully` );
		console.log(users);
	}
	else{
		res.send("Please input BOTH firstName and lastName");
	};
});

// PUT method
app.put("/change-lastName", (req, res) => {
	// initialized a variable to be used in the selection control structure codes
	let message;

	// loop through the elements of the "users" array
	for (let i = 0; i < users.length; i++){
		// checks if the user is existing in the array
		if(req.body.firstName == users[i].firstName){
			// will change the value of the lastName into a new lastName received from the request object
			users[i].lastName = req.body.lastName;

			message = `User ${req.body.firstName} has successfully changed the lastName into ${req.body.lastName}`;
			console.log(users);
			// will terminate the loop if there is a match
			break;
		// if no user is found...
		}else{
			message = "User does not exist"
		}
	}
	res.send(message);
})


//==================================================================
// ACTIVITY SECTION: 
//==================================================================


// 1. Create a GET route that will access the "/home" route that will print out a simple message.
// 2. Process a GET request at the "/home" route using postman.

app.get("/home", (req, res) => {
	res.send("Welcome to the home page");
});

// 3. Create a GET route that will access the "/users" route that will retrieve all the users in the mock database.
// 4. Process a GET request at the "/users" route using postman.
users = [
  {username: "johndoe", password: "johndoe1234"}
];
app.get("/users", (req, res) => {
	res.send(users);
});

// 5. Create a DELETE route that will access the "/delete-user" route to remove a user from the mock database.
// 6. Process a DELETE request at the "/delete-user" route using postman.

app.delete("/delete-user", (req, res) => {
	for (let i = 0; i < users.length; i++){
    if(users[i].username == req.body.username){
      users.splice(i,1);
      message =`User ${req.body.username} has been deleted`
    } else {
      message = `Cannot find username!`
    }
    res.send(message)
}});

// 7. Export the Postman collection and save it inside the root folder of our application.
// 8. Create a git repository named S34.
// 9. Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.
// 10. Add the link in Boodle.


// Tells our server to listen to the port
// if the port is accessed, we can run the server,
// returns a message to confirm that the server is running
app.listen(port, () => console.log(`Server running at port: ${port}`));
